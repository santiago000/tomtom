﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Enums
    {
       public enum UserType
        {
               Patient = 1,
               Doctor = 2
        };

        public enum DocumentType
        {
            CI = 1
        };
    }
}
