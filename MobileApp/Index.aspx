﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="MobileApp.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TomTom</title>

    <%--STYLES--%>
    <link href="Plugins/jquery.mobile-1.4.5/jquery.mobile.icons-1.4.5.min.css" rel="stylesheet" />
    <link href="Plugins/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css" rel="stylesheet" />
    <link href="Plugins/jquery.mobile-1.4.5/jquery.mobile.structure-1.4.5.min.css" rel="stylesheet" />

    <style>
    
        body {
            font-weight: normal !important;
        }

        .ui-bar-a, .ui-page-theme-a .ui-bar-inherit, html .ui-bar-a .ui-bar-inherit, html .ui-body-a .ui-bar-inherit, html body .ui-group-theme-a .ui-bar-inherit {
            border: none !important;
        }

        .ui-title .logoStart {
            border: none;
            background-color: #FFF !important;
        }

        .logoStart img {
            width: 80px;
        }

        .noshadow * {
            -webkit-box-shadow: none !important;
            -moz-box-shadow: none !important;
            box-shadow: none !important;
        }

        .nogradient,
        .nogradient * {
            background-image: none !important;
        }

        .nohighlight .ui-btn:before {
            display: none !important;
        }

        form.ui-mini .ui-field-contain fieldset.ui-controlgroup legend small {
            color: #666;
        }

        .new {
            position: absolute;
            bottom: 0;
            max-height: 45%;
        }
    </style>
    <link href="/Styles/style.css" rel="stylesheet" />
</head>
<body class="index-content" runat="server">
    <form id="form1" runat="server">
        <div data-role="header" role="banner" class="ui-header ui-bar-inherit">
            <h1 class="ui-title logoStart" role="heading" aria-level="1">
                <img class="popphoto" src="Images/logo.png">
            </h1>
        </div>
        <div role="main" class="ui-content jqm-content" style="margin-top: 10px;">
            <a href="#popupLogin" data-rel="popup" data-position-to="window" class="ui-btn ui-corner-all ui-shadow ui-icon-check ui-btn-icon-left" data-transition="pop">Ingresar</a>
            <div data-role="popup" id="popupLogin" data-theme="a" class="ui-corner-all">
                <div>
                    <div style="padding: 20px 30px;">
                        <label for="un" class="ui-hidden-accessible">Correo:</label>
                        <input type="text" name="user" id="un" value="" placeholder="Correo electrónico" data-theme="a">
                        <label for="pw" class="ui-hidden-accessible">Contraseña:</label>
                        <input type="password" name="pass" id="pw" value="" placeholder="Contraseña" data-theme="a">

                        <a href="Patients.aspx" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check">Ingresar</a>

                       <%-- <button type="submit" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check">Ingresar</button>--%>
                    </div>
                </div>
            </div>
            <a href="Register.aspx" data-transition="slidedown" class="ui-btn ui-corner-all">Registrarme</a>
        </div>
        <div role="main" class="ui-content jqm-content">
            <div class="jqm-block-content">
                <h2 class="section-title">Noticias</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
            </div>

        </div>
    </form>
    <%--SCRIPTS--%>
    <script src="Plugins/jquery/jquery.min.js"></script>
    <script src="Plugins/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
</body>
</html>
