﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="MobileApp.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TomTom</title>

    <%--STYLES--%>
    <link href="Plugins/jquery.mobile-1.4.5/jquery.mobile.icons-1.4.5.min.css" rel="stylesheet" />
    <link href="Plugins/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css" rel="stylesheet" />
    <link href="Plugins/jquery.mobile-1.4.5/jquery.mobile.structure-1.4.5.min.css" rel="stylesheet" />

    <style>
        body {
            font-weight: normal !important;
        }

        .ui-bar-a, .ui-page-theme-a .ui-bar-inherit, html .ui-bar-a .ui-bar-inherit, html .ui-body-a .ui-bar-inherit, html body .ui-group-theme-a .ui-bar-inherit {
            border: none !important;
        }

        .ui-overlay-a, .ui-page-theme-a, .ui-page-theme-a .ui-panel-wrapper {
            background-color: transparent !important;
        }

        .ui-header {
            background-color: transparent !important;
        }

        .ui-title .logoStart {
            border: none;
            background-color: #FFF !important;
        }

        .logoStart img {
            width: 80px;
        }

        .noshadow * {
            -webkit-box-shadow: none !important;
            -moz-box-shadow: none !important;
            box-shadow: none !important;
        }

        .nogradient,
        .nogradient * {
            background-image: none !important;
        }

        .nohighlight .ui-btn:before {
            display: none !important;
        }

        form.ui-mini .ui-field-contain fieldset.ui-controlgroup legend small {
            color: #666;
        }

        .new {
            position: absolute;
            bottom: 0;
            max-height: 45%;
        }
    </style>
    <link href="Styles/style.css" rel="stylesheet" />
</head>
<body class="index-content">
    <form id="form1" runat="server">
        <div role="main" class="ui-content jqm-content" style="margin-top: 10px;">
            <div class="ui-grid-a">
                <label for="name" class="ui-hidden-accessible">Nombre:</label>
                <input type="text" name="name" id="nam" value="" placeholder="Nombre" data-theme="a">
            </div>
            <div class="ui-grid-a">
                <label for="lastName" class="ui-hidden-accessible">Apellido:</label>
                <input type="text" name="lastName" id="lastName" value="" placeholder="Apellido" data-theme="a">
            </div>
            <div class="ui-grid-a">
                <label for="email" class="ui-hidden-accessible">Correo:</label>
                <input type="text" name="email" id="email" value="" placeholder="Correo" data-theme="a">
            </div>
            <div class="ui-grid-a">
                <label for="tel" class="ui-hidden-accessible">Telefono:</label>
                <input type="text" name="telephone" id="telephone" value="" placeholder="Tel&eacute;fono" data-theme="a">
            </div>
            <div class="ui-grid-a">
                <label for="select-specialty" class="ui-hidden-accessible">Seleccione opci&oacute;n:</label>
                <select name="select-specialty" id="specialty" data-mini="true" data-native-menu="false">
                    <option value="1">Cirujano</option>
                    <option value="2">Odontologo</option>
                    <option value="3" selected="selected">Imagin&oacute;logo</option>
                    <option value="4">Otro</option>
                </select>
            </div>
            <label for="select-clinic" class="ui-hidden-accessible">Seleccione opci&oacute;n:</label>
            <div class="ui-grid-a">
                <select name="select-clinic" id="clinic" data-mini="true" data-native-menu="false">
                    <option value="1">Tres Cruces</option>
                    <option value="2">Aguada</option>
                    <option value="3" selected="selected">Buceo</option>
                    <option value="4">Otro</option>
                </select>
            </div>
            <div class="ui-grid-a">
                <label for="password" class="ui-hidden-accessible">Contrase&ntilde;a:</label>
                <input type="password" name="pasword" id="password" value="" placeholder="Contrase&ntilde;a" data-theme="a">
            </div>
            <div class="ui-grid-a">
                <label for="rep-password" class="ui-hidden-accessible">Repite Contrase&ntilde;a:</label>
                <input type="password" name="rep-pasword" id="rep-password" value="" placeholder="Confirma Contrase&ntilde;a" data-theme="a">
            </div>
        </div>
        <button type="submit" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check">Ingresar</button>
    </form>
    <%--SCRIPTS--%>
    <script src="Plugins/jquery/jquery.min.js"></script>
    <script src="Plugins/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <script type="text/javascript">
        $('#specialty').on('change', function () {
            debugger;
            console.log('click specialty');
            //event.preventDefault();
        });
        //$(window).on("navigate", function (event) {
        //    console.log("navigated!");
        //    event.preventDefault();
        //});
    </script>
</body>
</html>
