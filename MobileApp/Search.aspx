﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TomTom.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="MobileApp.Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div role="main" class="ui-content jqm-content">
        <div class="jqm-block-content">
            <div data-demo-html="true">
                <label for="search">Buscar pacientes:</label>
                <input type="text" data-type="search" name="password" id="search" value="" placeholder="Documento o nombre">
            </div>
            <a href="/Patients.aspx" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-search">Buscar</a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
