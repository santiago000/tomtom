﻿using DTO;
using Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DTO.Enums;

namespace Business
{
    public class UsersBusiness
    {
        public List<UserDTO> Get(UserType userType)
        {
            return Persistence.UsersPersistence.GetInstance().Get(userType);
        }
    }
}
