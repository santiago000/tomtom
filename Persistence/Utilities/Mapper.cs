﻿using DTO;
using Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Utilities
{
    public class Mapper
    {
        public static UserDTO Map(Users user)
        {
            return new UserDTO { Name = user.Name };
        }

        public static List<UserDTO> Map(List<Users> users)
        {
            List<UserDTO> output = new List<UserDTO>();
            foreach (Users u in users)
            {
                output.Add(Map(u));
            }
            return output;
        }
    }
}
