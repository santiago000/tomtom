﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using static DTO.Enums;

namespace Persistence
{
    public class UsersPersistence
    {
        private static UsersPersistence _instance;

        private UsersPersistence() { }

        public static UsersPersistence GetInstance()
        {
            return _instance == null ? new UsersPersistence() : _instance;
        }

        public List<UserDTO> Get(UserType userType)
        {
            using (var context = new Persistence.Model.TomTomEntities())
            {
                var users = context.Users.Where(x => x.UserTypeId == (int)userType).ToList();
                return users != null ? Utilities.Mapper.Map(users) : null;
            }
        }
    }
}
